<!doctype html>

<?require_once ("ink/config.php");?>

<html ="no-js" lang="en">
  <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulaire</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/app.css">
        <link href="https://fonts.googleapis.com/css?family=Acme|Passion+One|Russo+One" rel="stylesheet">
        <script src="https://use.fontawesome.com/89e1625d3c.js"></script>
  </head>
	<body>
		<div class="login">
			<form action="loginOn.php" method="post">

				<i class="fa fa-user" aria-hidden="true"></i>
  				User:<br>
  				<input type="text" name="user" value="User">
  				<br><br>

  				<i class="fa fa-envelope" aria-hidden="true"></i>
  				Email:<br>
  				<input type="text" name="email" value="@gmail.com">
  				<br><br>

  				<i class="fa fa-unlock" aria-hidden="true"></i>
  				Password:<br>
  				<input type="Password" name="password" value="Password">
  				<br><br>

          		<div class="submit">
  				  <input type="submit" value="Login">
  				  <br>
          		</div>
			</form> 	
		</div>
    	<script src="bower_components/jquery/dist/jquery.js"></script>
    	<script src="bower_components/what-input/dist/what-input.js"></script>
    	<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    	<script src="js/app.js"></script>
	</body>
</html>
		
