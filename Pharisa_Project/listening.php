<?php
    require_once ("ink/config.php");
    
?>

<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Project manager</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <div class="container">
            
            <?php require_once 'template/header.php';?>
            <main>
                <ul class="projectlist">

                    <li class="projectlist-item">
                        <span class="item-name">Name</span>
                        <span class="item-month">Janvier</span>
                        <span class="item-month">Février</span>
                        <span class="item-month">Mars</span>
                        <span class="item-month">Avril</span>
                        <span class="item-month">Mai</span>
                        <span class="item-month">Juin</span>
                        <span class="item-month">Juillet</span>
                        <span class="item-month">Août</span>
                        <span class="item-month">Septembre</span>
                        <span class="item-month">Octobre</span>
                        <span class="item-month">Novembre</span>
                        <span class="item-month">Décembre</span>  
                    </li>

                    <?php foreach( $data as $row) :?>

                    <li class="<?php if($row['start'] == 'start'):?>isstart<?php endif;?>">

                        <span class="item-name"><?php echo $row['name']?></span>

                        <span class="item-month"><?php echo $row['start']?></span>

                        <span class="item-month"><?php echo $row['start']?></span>
                        <span class="item-month"><?php echo $row['start']?></span>
                        <span class="item-month"><?php echo $row['start']?></span>
                        <span class="item-month"><?php echo $row['start']?></span>
                        <a href="delete.php?project=<?php echo $row['id'];?>">Supprimer</a>
        
                    </li> 
                    <?php endforeach;?> 
                </ul> 
               <p class="plus"><a href="edit.php"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></p> 
            </main>
            <?php require_once 'template/footer.php';?>
        </div>
 	</body>
</html>